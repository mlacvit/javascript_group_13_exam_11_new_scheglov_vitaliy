import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HomeComponent } from './home/home.component';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { MatInputModule } from '@angular/material/input';
import { BoomComponent } from './boom/boom.component';
import { NewComponent } from './new/new.component';
import { localStorageSync } from 'ngrx-store-localstorage';
import { boomReducer } from './Store/boom.reducer';
import { userReducer } from './Store/user.reducer';
import { BoomEffects } from './Store/boom.effects';
import { UserEffects } from './Store/user.effects';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{user: ['user']}],
    rehydrate: true
  })(reducer)
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer]

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    FileInputComponent,
    RegistrationComponent,
    LoginComponent,
    BoomComponent,
    NewComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatProgressBarModule,
        MatSnackBarModule,
        LayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        StoreModule.forRoot({boom: boomReducer, user: userReducer}, {metaReducers}),
        EffectsModule.forRoot([BoomEffects, UserEffects]),
        MatInputModule,
        MatCardModule,
        MatMenuModule,
        MatSelectModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
