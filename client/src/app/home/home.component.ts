import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BoomModel } from '../Model/Boom.model';
import { AppState } from '../Store/types';
import { Store } from '@ngrx/store';
import { fetchBoomReq } from '../Store/boom.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  boom: Observable<BoomModel[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.boom = store.select(state => state.boom.boom);
    this.loading = store.select(state => state.boom.loading);
    this.error = store.select(state => state.boom.error);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchBoomReq());
  }
}
