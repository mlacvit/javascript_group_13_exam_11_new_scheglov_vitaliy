import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../Store/types';
import { Observable } from 'rxjs';
import { User } from '../../Model/user.model';
import { logOut, logOutReq } from '../../Store/user.actions';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  user: Observable<null | User>
  username!: User | null
  constructor(private store: Store<AppState>) {
    this.user = store.select(store => store.user.user)
  }

  logOut(){
    this.store.dispatch(logOutReq());
    this.store.dispatch(logOut());
  }

  ngOnInit(): void {
    this.user.forEach(user => {
      return this.username = user;
    })
  }
}
