import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { LoginData, LoginError } from '../Model/user.model';
import { AppState } from '../Store/types';
import { Store } from '@ngrx/store';
import { loginReq } from '../Store/user.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent  {
  @ViewChild('f') form!: NgForm;
  error: Observable<null | LoginError>;
  loading: Observable<boolean>;
  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.user.loginError);
    this.loading = store.select(state => state.user.loginLoad);
  }

  onLogin() {
    const userData: LoginData = this.form.value;
    this.store.dispatch(loginReq({userData}));
  }
}
