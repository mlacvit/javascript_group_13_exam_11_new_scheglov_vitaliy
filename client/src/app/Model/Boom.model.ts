import { User } from './user.model';

export class BoomModel {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public category: string,
    public image: string,
    public price: number,
    public user: User,
  ) {}
}


export interface BoomData {
  title: string;
  description: string,
  category: string,
  image: File,
  price: string
}
