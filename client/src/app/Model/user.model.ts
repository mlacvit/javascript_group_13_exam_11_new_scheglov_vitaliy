export interface User {
  _id: string,
  email: string,
  username: string,
  image: string,
  token: string,
}

export interface UserData {
  email: string,
  username: string,
  password: string,
  image: File | null;
}

export interface LoginData {
  email: string,
  password: string,
}

export interface ErrorField{
  message: string
}

export interface LoginError{
  error: string
}

export interface UserError {
  errors: {
    email: ErrorField,
    username: ErrorField,
    password: ErrorField,
  }
}
