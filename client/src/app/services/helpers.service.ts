import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { ActionType } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { of , catchError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor(private snackBar: MatSnackBar) { }

  openSnack(message: string, action?: string, config?: MatSnackBarConfig){
    if (!config || !config.duration){
      config = {...config, duration: 5000}
    }if (!action) {
      action = 'ok'
    }return this.snackBar.open(message, action, config);
  }

  catchError(action: ActionType<any>){
    return catchError(regErr => {
      let validationError = null;
      if (regErr instanceof HttpErrorResponse && regErr.status === 400){
        validationError = regErr.error;
      }else {
        this.snackBar.open('server error', 'ok', {duration: 5000})
      }
      return of(action({error: validationError}))
    })
  }
}
