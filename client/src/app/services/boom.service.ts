import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BoomData, BoomModel } from '../Model/Boom.model';

@Injectable({
  providedIn: 'root'
})
export class BoomService {
  constructor(private http: HttpClient) {}

  getBoom() {
    return this.http.get<BoomModel[]>('http://localhost:8000/boom');
  }
  createBoom(postData: BoomData, token: string){
    return this.http.post('http://localhost:8000/boom', postData,{
      headers: new HttpHeaders({'Authorization': token})
    });
  }
  tokenAuthorization(token: string){
    return this.http.post('http://localhost:8000/boom', {
      headers: new HttpHeaders({'Authorization': token})
    });
  }
}
