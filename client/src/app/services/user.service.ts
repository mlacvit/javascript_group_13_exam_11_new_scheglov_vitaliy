import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginData, User, UserData } from '../Model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  registerUser(userData: UserData){
    return this.http.post<User>('http://localhost:8000/user', userData);
  }

  loginUser(userData: LoginData){
    return this.http.post<User>('http://localhost:8000/user/sessions', userData);
  }

  LogOut(token: string){
    return this.http.delete('http://localhost:8000/user/sessions', {
      headers: new HttpHeaders({'Authorization': token})
    });
  }


}
