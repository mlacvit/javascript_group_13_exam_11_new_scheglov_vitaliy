import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../Store/types';
import { BoomData } from '../Model/Boom.model';
import { createBoomReq } from '../Store/boom.actions';
import { User } from '../Model/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.sass']
})
export class NewComponent  {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.boom.createLoading);
    this.error = store.select(state => state.boom.createError);
    this.user = store.select(state => state.user.user);
  }

  onSub() {
    const boomData: BoomData = this.form.value;
    let token = '';
    this.user.forEach(user => {
      if (!user){
        return null
      }
    return token = (user?.token);
    })
    this.store.dispatch(createBoomReq({boomData, token}));
  }

}
