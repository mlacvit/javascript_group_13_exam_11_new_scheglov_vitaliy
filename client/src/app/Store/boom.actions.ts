import { createAction, props } from '@ngrx/store';
import { BoomData, BoomModel } from '../Model/Boom.model';

export const fetchBoomReq = createAction('[Boom] Boom Req');
export const fetchBoomSusses = createAction(
  '[Boom] Boom susses',
  props<{boom: BoomModel[]}>()
);
export const fetchBoomFail = createAction(
  '[Boom] Boom Fail',
  props<{error: string}>()
);

export const createBoomReq = createAction(
  '[Boom] create Req',
  props<{boomData: BoomData, token: string}>()
);
export const createBoomSusses = createAction(
  '[Boom] create susses');
export const createBoomFail = createAction(
  '[Boom] Post Fail',
  props<{error: string}>()
);

