import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BoomService } from '../services/boom.service';
import { Router } from '@angular/router';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  createBoomFail,
  createBoomReq,
  createBoomSusses,
  fetchBoomFail,
  fetchBoomReq,
  fetchBoomSusses,
} from './boom.actions';

@Injectable()
export class BoomEffects {
  fetchBoom = createEffect(() => this.actions.pipe(
    ofType(fetchBoomReq),
    mergeMap(() => this.service.getBoom().pipe(
      map(boom => fetchBoomSusses({boom})),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(fetchBoomFail({
        error: 'Something went wrong'
      })))
    ))
  ));

  createPostEffect = createEffect(() => this.actions.pipe(
    ofType(createBoomReq),
    mergeMap(({boomData, token}) => this.service.createBoom(boomData, token).pipe(
      map(() => createBoomSusses()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createBoomFail({error: 'wrong data'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private service: BoomService,
    private router: Router
  ) {}
}
