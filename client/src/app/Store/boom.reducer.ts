import { BoomState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createBoomFail,
  createBoomReq,
  createBoomSusses,
  fetchBoomFail,
  fetchBoomReq,
  fetchBoomSusses,
} from './boom.actions';

const initialState: BoomState = {
  boom: [],
  loading: false,
  error: null,
  createLoading: false,
  createError: null
};

export const boomReducer = createReducer(
  initialState,
  on(fetchBoomReq, state => ({...state, loading: true})),
  on(fetchBoomSusses, (state, {boom}) => ({
    ...state,
    loading: false,
    boom
  })),
  on(fetchBoomFail, (state, {error}) => ({
    ...state,
    loading: false,
    fError: error
  })),

  on(createBoomReq, state => ({...state, loading: true})),
  on(createBoomSusses, state => ({
    ...state,
    loading: false,
  })),
  on(createBoomFail, (state, {error}) => ({
    ...state,
    loading: false,
    fError: error
  })),

);
