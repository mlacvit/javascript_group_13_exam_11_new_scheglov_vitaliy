import { LoginError, User, UserError } from '../Model/user.model';
import { BoomModel } from '../Model/Boom.model';


export type BoomState = {
  boom: BoomModel[],
  loading: boolean,
  error: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type AppState = {
  boom: BoomState,
  user: UserState
}

export type UserState = {
  user: null | User,
  regload: boolean,
  regError: null | UserError,
  loginLoad: boolean,
  loginError: null | LoginError
}
