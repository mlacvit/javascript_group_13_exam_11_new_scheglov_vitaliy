import { createAction, props } from '@ngrx/store';
import { LoginData, LoginError, User, UserData, UserError } from '../Model/user.model';

export const regUserReq = createAction(
  '[Users] users req' ,
  props<{userData: UserData}>()
);
export const regUserSus = createAction(
  '[Users] users sus',
  props<{user: User}>()
);
export const regUserFai = createAction(
  '[Users] users fai' ,
  props<{error: null | UserError}>()
);

export const loginReq = createAction(
  '[Users] login req' ,
  props<{userData: LoginData}>()
);
export const loginSus = createAction(
  '[Users] login sus',
  props<{user: User}>()
);
export const loginFai = createAction(
  '[Users] login fai' ,
  props<{error: null | LoginError}>()
);

export const logOut = createAction(
  '[Users] logOut');
export const logOutReq = createAction(
  '[Users] logOut req');
