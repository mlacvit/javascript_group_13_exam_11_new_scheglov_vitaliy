import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { loginFai, loginReq, loginSus, logOut, logOutReq, regUserFai, regUserReq, regUserSus } from './user.actions';
import { map, mergeMap, NEVER, tap, withLatestFrom } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from './types';
import { HelpersService } from '../services/helpers.service';

@Injectable()

export class UserEffects {
  constructor(
    private actions: Actions,
    private service: UserService,
    private helpers: HelpersService,
    private router: Router,
    private store: Store<AppState>,
  ) {}

  registerUser = createEffect(() => this.actions.pipe(
    ofType(regUserReq),
    mergeMap(({userData}) => this.service.registerUser(userData).pipe(
      map(user => regUserSus({user})),
      tap(() => {
        this.helpers.openSnack('register successful');
        void this.router.navigate(['/'])
      }),
      this.helpers.catchError(regUserFai)
    ))
  ))

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginReq),
    mergeMap(({userData}) => this.service.loginUser(userData).pipe(
      map(user => loginSus({user})),
      tap(() => {
        this.helpers.openSnack('login successful');
        void this.router.navigate(['/'])
      }),
      this.helpers.catchError(loginFai)
    ))
  ))

  logOut = createEffect(() => this.actions.pipe(
    ofType(logOutReq),
   withLatestFrom(this.store.select(state => state.user.user)),
    mergeMap(([_, user]) => {
      if (user){
        return this.service.LogOut(user.token).pipe(
          map(() => logOut()),
          tap(() => this.helpers.openSnack('logout successful'))
        );
      }
     return NEVER;
    }))
  )
}
