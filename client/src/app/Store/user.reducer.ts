import { UserState } from './types';
import { createReducer, on } from '@ngrx/store';
import { loginFai, loginReq, loginSus, logOut, regUserFai, regUserReq, regUserSus } from './user.actions';

const initialState: UserState = {
  user: null,
  regload: false,
  regError: null,
  loginLoad: false,
  loginError: null
};

export const userReducer = createReducer(
  initialState,
  on(regUserReq, state => ({...state, regload: true, regError: null})),
  on(regUserSus, (state, {user}) => ({
    ...state,
    regload: false,
    user
  })),
  on(regUserFai, (state, {error}) => ({
    ...state,
    regload: false,
    regError: error
  })),

  on(loginReq, state => ({
    ...state,
    loginLoad: true,
    loginError: null
  })),
  on(loginSus, (state, {user}) => ({
    ...state,
    loginLoad: false,
    user
  })),
  on(loginFai, (state, {error}) => ({
    ...state,
    loginLoad: false,
    loginError: error
  })),
  on(logOut, state => ({
    ...state,
    user: null,
  }))
)
