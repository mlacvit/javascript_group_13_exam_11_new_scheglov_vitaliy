import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { regUserReq } from '../Store/user.actions';
import { UserError } from '../Model/user.model';
import { AppState } from '../Store/types';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements AfterViewInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  error: Observable<null | UserError>;
  loading: Observable<boolean>;
  errorSub!: Subscription;
  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.user.regError);
    this.loading = store.select(state => state.user.regload);
  }

  onSubRegister() {
    this.store.dispatch(regUserReq({userData: this.form.value}))
  }

  ngAfterViewInit(): void {
    this.errorSub = this.error.subscribe(error => {
      if (error){
        const msg = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: msg})
      }else {
        this.form.form.get('email')?.setErrors({})
      }
    })
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }
}
