const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BoomSchema = new Schema({

  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: String,
  price: Number,
  category: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Boom = mongoose.model('Boom', BoomSchema);

module.exports = Boom;